﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private Rigidbody rb;

    public bool atract;

    public float speed;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        atract = true;
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);

        rb.AddForce(movement * speed);

        if (Input.GetKeyDown("space"))
        {
           atract = !atract;
        }
    }

    private void LateUpdate()
    {
        if (atract == true)
        {
            GetComponent<Renderer>().material.color = new Color(1, 0, 0);
        }
        else
        {
            GetComponent<Renderer>().material.color = new Color(0, 0, 1);
        }
    }
}