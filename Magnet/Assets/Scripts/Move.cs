﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    private Rigidbody other;

    public float speed;
    public Transform target;

    private void Start()
    {
        other = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float step = speed * Time.deltaTime;

        if (target.GetComponent<Player>().atract == true)
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        else
            transform.position = Vector3.MoveTowards(transform.position, target.position, -step);
    }
}